<?php
  require_once '../../database/dbhandler.php';

  $db = new DbHandler();
  $sql = "SELECT * FROM contacts";
  $result = $db->getMysqli()->query($sql);

  if ($result->num_rows > 0) {
    // output data of each row
    $jsonData = array();
    while($row = $result->fetch_assoc()) {
        $jsonData[] = $row;
    }
    echo json_encode($jsonData);
  } else {
      echo "0 results";
  }
 ?>
