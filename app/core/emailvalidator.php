<?php

require_once '../../database/dbhandler.php';

if(isset($_POST)){
  $db = new DbHandler();

  //email validation - show error page if invalid email
  $email = test_input($_POST["email"]);
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Invalid email format";
    echo '<script>console.log( "Debug: ' . $emailErr .'");</script>'; //DEBUG
    require_once '../views/home/error.php';
  }
  else {
    $db->insertEmailToTable($_POST["email"]);
    require_once '../views/home/home.php';
  }


}

//validation function
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
