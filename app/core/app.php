<?php


class App {

  /*
  * @desc initial state of app, showing default view and creating contacts table
  */
  public function __construct() {

      //showing initial view
      require_once '../app/views/home/index.php';

      $db = new DbHandler();

      //Creating contacts table when the app starts (if does not exist already)
      $db->createContactsTable();


      //unset($db); //not necessary because of garbage collection
  }


}

?>
