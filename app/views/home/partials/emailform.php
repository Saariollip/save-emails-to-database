<div class="panel-heading">Email</div>
<div class="panel-body">
      <form class="form-horizontal" role="form" method="POST" action="../app/core/emailvalidator.php" >

          <div class="form-group">
              <label for="email" class="col-md-3 control-label">Email:</label>

              <div id="email_field" class="col-md-7 input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i>          </span>
                    <input id="email" type="email" class="form-control" name="email" required placeholder="Enter a valid email address">
              </div>

          </div>


            <div class="form-group">
              <div class="col-md-12">
               <button type="submit" class="btn btn-primary">
                   <i class="fa fa-btn fa-send"></i> Send
               </button>
             </div>
           </div>

    </form>
</div>
