# Save emails to database #

This PHP app validates and saves emails to mysql database. It is also possible to check emails as json format after the email has been submitted.


### How do I get set up? ###

* Summary of set up

  1. Clone the repository
  
  2. run with LAMP/XAMPP
  
  3. setup MySql database and connect it to the app in database/dbhandler.php
  
#### Dependencies

  * PHP (> 5.6) and MySQLi
  
#### Database configuration

  * Test database configuration is hardcoded in database/dbhandler.php for easier
  demonstration. Database table is created automatically when the app starts.
  
#### How to run tests

  * There are no unit tests. App writes part of the log to browser's console during execution.
