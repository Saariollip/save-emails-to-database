<?php

/* globals only for easier demonstration */
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', 'secret2357');
define('DB_NAME', 'db');

/*
* @author Olli-Pekka Saari
* Class for making DB connection, creating table and inserting data to table
*/

class DbHandler {

  //mysqli instance
  private $mysqli;

  public function __construct() {
    // Connect to MySql
    $this->mysqli = new mysqli (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  }

  /*
  * @desc getter (no magic methods)
  */
  public function getMysqli() {
    return $this->mysqli;
  }

  /*
  * @desc creates contacts table
  */
  public function createContactsTable() {


    // Check connection
    $this->checkConnection();

    $sql = "CREATE TABLE contacts (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      email VARCHAR(50),
      reg_date TIMESTAMP
    )";

    if ($this->mysqli->query($sql) === TRUE) {
        echo '<script>console.log( "Debug:  Table contacts created succesfully!");</script>';
    } else {
        echo '<script>console.log( "Debug: ' . $this->mysqli->error .'");</script>'; //DEBUG
    }

  }

  /*
  * @desc connects to database and stores the data to contacts table
  * @param value which is stored to database
  */
  public function insertEmailToTable($value) {

    //check db connection
    $this->checkConnection();

		//Insert data
		$sql = "INSERT INTO contacts (email) VALUES ( '{$this->mysqli->real_escape_string($value)}' )";
		$insert = $this->mysqli->query($sql);

    // if inserted succesfully
		if ($insert) {
        echo '<script>console.log( "Debug:  data: ' . $value .' inserted to database succesfully!");</script>';
    }
		else {
			die("Error: {$this->mysqli->errno} : {$this->mysqli->error}");
		}


  }

  /*
  * @desc closing the database connection when object is destroyed
  */
  public function __destruct() {
    $this->mysqli->close();
  }

  public function checkConnection() {

    if ($this->mysqli->connect_error){
      die ( 'Connect Error: ' . $this->mysqli->connect_errno . ' : ' .  $this->$mysqli->connect_error );
      // Connect if there is no connection already
      $this->$mysqli = new mysqli (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    }

  }
}
 ?>
